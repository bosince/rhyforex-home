<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RHYBIT- 联系我们</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">

    <style>
        .page {
            margin-top: 80px;
        }
        .page > nav {
            line-height: 80px;
            border-bottom: 1px solid #ccc;
        }
        .page > nav  ol.breadcrumb {
            margin-bottom: 0;
            padding: 0;
        }
        
        .sub-nav .topul {
            border-bottom: 3px solid #820f1a;
        }
        .sub-nav .topul > li{
            line-height: 60px;
            background: #cb1224;
            color: #fff;
            font-size: 14px;
            font-weight: bold;
            padding-left: 15px;
            /*border-radius: 3px;*/
        }
        .sub-nav .subul li a{
            display: block;
            line-height: 50px;
            color: #666;
            padding-left: 15px;
            border-top: 1px solid #fff;
            border-bottom: 1px solid #fff;
            text-decoration: none;
        }
        .sub-nav .subul li a:hover {
            background: #cb1224;
            color: #fff;
        }
        .main {
            padding: 50px 0;
        }
        .left {
            text-align: center;
        }
        .left .img2 {
            margin-top: 50px;
        }
        
        .left h4 {
            font-size: 30px;
            font-weight: bold;
        }

        @media (max-width: 768px) {
            .page {
                margin-top: 0;
            }

        }
    </style>

</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <nav>
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="/">首页</a></li>
                    <li><a href="/contact.php">客户需知</a></li>
                    <li class="active">联系我们</li>
                </ol>
            </div>
        </nav>
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">

                        <div class="left">
                            <img src="assets/img/icon_1.png" height="52" width="52" alt="">
                            <h4>邮箱</h4>
                            <p>邮箱：info@rhybit.com</p>
                            <p>邮箱：support@rhybit.com</p>

                            <img src="assets/img/icon_2.png" height="52" width="52" alt="" class="img2">
                            <h4>地址</h4>
                            <p>地址：香港尖沙咀柯士甸大道</p>

                        </div>

                    </div>

                    <div class="col-sm-3 col-sm-offset-1 hidden-xs sub-nav">
                        <?php include 'sub-nav.html' ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script> -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script> -->

    <!-- <script src="assets/js/common.js"></script> -->
</body>
</html>