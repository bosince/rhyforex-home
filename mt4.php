<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RHYBIT - MT4</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">

    <style>
        .page {
            margin-top: 80px;
        }
        .page > nav {
            line-height: 80px;
            border-bottom: 1px solid #ccc;
        }
        .page > nav  ol.breadcrumb {
            margin-bottom: 0;
            padding: 0;
        }


        .main .row {
            padding: 50px 0;
        }
        .main h3 {
            font-size: 25px;
            margin-bottom: 15px;
            letter-spacing: 3px;
        }
        .main p {
            font-size: 16px; 
        }
        
        .main ul li {
            font-size: 16px;
            line-height: 28px;
        }
        .imgWrap {
            display: flex;
            align-items: flex-end;
        }
        .imgWrap > div {
            display: flex;
            flex-flow: column wrap;
            justify-content: center;
            padding: 20px 20px 20px 0;
        }
        .imgWrap img {
            width: 150px;
        }
        .imgWrap a {
            display: inline-block;
            margin-top: 15px;
            width: 150px;
            line-height: 50px;
            text-align: center;
            color: #fff;
            border-radius: 3px;
            background: #c01a28;
            text-decoration: none;
        }


        @media (max-width: 768px) {
            .page {
                margin-top: 0;
            }
            .main .row {
                padding: 30px 0;
            }
        }
    </style>

</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <nav>
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="/">首页</a></li>
                    <li><a href="/mt4.php">交易软件</a></li>
                    <li class="active">MT4</li>
                </ol>
            </div>
        </nav>
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>MT4 独特 高效 专业</h3>
                        <p>目前国际上最主流的交易平台之一，多语言支持</p>
                        <ul>
                            <li>一个平台，多种产品</li>
                            <li>即时报价，及时买卖</li>
                            <li>支持多种订单管理</li>
                            <li>安全可靠，24小时交易</li>
                            <li>EA智能交易</li>
                        </ul>
                        <div class="imgWrap">
                            <div>
                                <img src="assets/img/IOS.png" alt="">
                                <a href="https://download.mql5.com/cdn/mobile/mt4/ios?hl=zh&utm_campaign=download.mt4.ios&pt=420190&utm_source=mt4terminal&ct=mt4terminal&server=Rhine-Default">IOS下载</a>
                            </div>
                            <div>
                                <!-- <img src="assets/img/Android.png" alt=""> -->
                                <a href="download/metatrader4.apk">Android下载</a>
                            </div>
                            <div class="hidden-xs">
                                <!-- <img src="assets/img/ios_mt4.png" alt=""> -->
                                <a href="https://download.mql5.com/cdn/web/10967/mt4/rhinegroup4setup.exe">PC 下载</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <img src="assets/img/downApp.png" alt="">
                    </div>
                </div>

<!--                 <div class="row">
                    <div class="col-sm-6">
                        <img src="assets/img/trader.png" alt="">
                    </div>
                    <div class="col-sm-6">
                        <h3>RHY Trader平台</h3>
                        <p>RHYTrader 平台是RHY专属研发的外汇交易平台，符合大陆交易者习惯。我们研发的位于移动端的应用APP，让客户随时随地，畅快交易。无论你是交易新手还是专家，都能满足你的需求。</p>
                        <ul>
                            <li>优质的深度交易系统</li>
                            <li>层级保证金交易方式</li>
                            <li>移动端采用指纹解锁系统</li>
                            <li>两种交易模式自如切换</li>
                        </ul>
                    </div>
                </div> -->
            </div>
        </div>
        
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script> -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script> -->

    <!-- <script src="assets/js/common.js"></script> -->
</body>
</html>