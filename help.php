<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RHYBIT - MT4</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">

    <style>
        .page {
            margin-top: 80px;
        }
        .page > nav {
            line-height: 80px;
            border-bottom: 1px solid #ccc;
        }
        .page > nav  ol.breadcrumb {
            margin-bottom: 0;
            padding: 0;
        }

        .main p {
            margin-top: 15px;
            font-size: 14px;
        }
        .main a {
            font-size: 14px;
            color: #666;
        }
        .main img {
            /*max-width: 800px;*/
            margin: 15px 0 30px;
        }
        .main h2 {
            margin: 30px 0 15px;
            font-size: 30px;
        }
        .main h4 {
            font-size: 16px;
        }
        .main h5 {
            margin: -20px 0 20px 15px;
        }

        .main .topul {
            position: fixed;
            top: 180px;
            width: 200px;
            border-bottom: 3px solid #820f1a;
            height: 100vh;

        }
        .main .topul > li{
            line-height: 60px;
            background: #cb1224;
            color: #fff;
            font-size: 14px;
            font-weight: bold;
            padding-left: 15px;

        }
        .main .subul li a{
            display: block;
            line-height: 50px;
            color: #666;
            padding-left: 15px;
            border-top: 1px solid #fff;
            border-bottom: 1px solid #fff;
            text-decoration: none;
            background: rgba(155,155,155,.1);
        }
        .main .subul li a:hover {
            background: #cb1224;
            color: #fff;
        }



        @media (max-width: 768px) {
            .page {
                margin-top: 0;
            }
            
        }
    </style>

</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <nav>
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="/">首页</a></li>
                    <li class="active">新手必看</li>
                </ol>
            </div>
        </nav>

        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10">
                        <h2>新手必看</h2>
                        <p>最近有很多用户都在问现在如何用人民币炒币，其实方法很简单，只需三步：买币、交易、卖币！</p>

                        <p>人民币买币视频教程如下: <a href="http://v.youku.com/v_show/id_XMzIyMjQ4MTY4MA==.html">http://v.youku.com/v_show/id_XMzIyMjQ4MTY4MA==.html</a> </p>

                        <p>卖出提现人民币视频教程如下：<a href="http://v.youku.com/v_show/id_XMzIyMjUyOTk0OA==.html">http://v.youku.com/v_show/id_XMzIyMjUyOTk0OA==.html</a></p>

                        <p>话不多说，接下来也图文详解一下，具体该如何用人民币买币，如何转到场内交易，又如何将币卖出变现！</p>

                        <!-- <img src="assets/img/help/1-01.png" alt=""> -->

                        <h4>一：如何买币？</h4>
                        <p>1.登录<a href="https://www.huobi.br.com/zh-cn/">https://www.huobi.br.com/zh-cn/</a> 找到“法币交易”，点击进入。</p>

                        <img src="assets/img/help/1-02.png" >

                        <p>2. 进入如下页面，选择“交易中心”-“我要买入”就可以看到很多正在出售的USDT，它们有不同的价格，不同的付款方式。找一个符合自己心理价位的，选择买入即可。</p>
                        <img src="assets/img/help/1-03.png" >

                        <p>3. 选择一个合适的价格，点击买入后，会进入如下购买页面。</p>
                        <img src="assets/img/help/1-04.png" >

                        <p>4. 输入你要购买的金额，然后确认买入，即可成功创建购买订单。</p>
                        <img src="assets/img/help/1-05.png" >

                        <p>5. 创建订单后，需要与卖家联系，确认卖家在线后，在付款期限内打款至广告主提供的支付宝/微信/银行账户。转账时尽量备注下图所示的付款参考号，这样卖家方便看到是你的付款，然后付款后点击“付款已完成”就可以了。</p>
                        <img src="assets/img/help/1-06.jpg" >
                        <h5>温馨提示：与卖家联系时，请务必确认卖家在线，方可打款交易；付款完成后，要及时点击“付款已完成”。</h5>

                        <p>6. 卖家收到款项后会确认收款，然后系统就会自动把数字资产打到你的账户，到时候记得查收哦。</p>
                        <img src="assets/img/help/1-07.png" >
                        <h5>温馨提示：如果你已打款，但卖家却迟迟不回复，要在订单有限期限内及时申诉。</h5>

                        <p>7. 恭喜你，已经成功买到USDT。BTC的购买流程也是一样的，按照同样的方式，就可以购买BTC了。</p>
                        <img src="assets/img/help/1-08.png" >



                        <!-- <h4>二、如何交易？</h4>
                        <p>1. 登录<a href="www.huobi.pro">www.huobi.pro</a> 或 <a href="https://www.huobipro.com">https://www.huobipro.com</a>，找到“法币交易”，点击进入。</p>
                        <img src="assets/img/help/2-01.png">

                        <p>2.点击“资金管理”后可以看到如下页面，再点击“划转至火币Pro”即可实时免费将币划转至火币Pro。</p>
                        <img src="assets/img/help/2-02.jpg">
                        
                        <p>3.选择从OTC站账户⇌专业站账户，输入需要划转的BTC或USDT的数量，点击确定即可成功划转。</p>
                        <img src="assets/img/help/2-03.jpg">
                        
                        <p>4.划转完成后就可以登录火币Pro站（www.huobi.pro），在资金管理里面查看自己刚刚划转的USDT，然后就可以在Pro站交易了。</p>
                        <img src="assets/img/help/2-04.jpg">

                        <p>5.比如你想用USDT去兑换最近行情走好的BTC，首先点击“币币交易”，选择BTC/USDT的交易对，点击后就可以进入这个交易对的交易页面。</p>
                        <img src="assets/img/help/2-05.jpg">

                        <p>然后可以根据自己的需要，来选择限价/市价交易。所谓的限价交易就是挂单交易，你可以自己设定买入/卖出价格，等市场价格波动到自己设定的价格便可成交。</p>
                        <img src="assets/img/help/2-06.jpg">

                        <p>所谓的市价交易就是指不设定买入价格，按照当时的市场价格买入或卖出，只需输入想买入的总金额或卖出的总量即可。</p>
                        <img src="assets/img/help/2-07.jpg"> -->

                        
                        <h4>二、如何卖出提现人民币？</h4>
                        <p>1. 登录<a href="https://www.huobi.br.com/zh-cn/">https://www.huobi.br.com/zh-cn/</a>，找到“法币交易”，点击进入。</p>
                        <img src="assets/img/help/3-01.png">

                        <p>2. 进入如下页面，先点击“资产管理”，再点击“BTC或USDT”，选择相应币种查看充币地址，将币充值到点对点交易平台。比如：你想将BTC卖出变现，那就查看BTC的充币地址，将币充值到这个地址；如果你想将USDT卖出变现，那就查看USDT的充币地址，将币充值到这个地址。</p>
                        <img src="assets/img/help/3-02.png">

                        <p>3. 充值完成后，币就转到了火币OTC平台。在这里，以USDT为例，点击进入“交易中心”，会进入如下页面。点击 “我要卖出”就可以看到很多正在收购USDT的买家，它们有不同的价格，不同的付款方式。找一个符合自己心理价位的，选择卖出即可。</p>
                        <img src="assets/img/help/3-03.png">

                        <p>4. 选择一个合适的价格，点击卖出后，会进入如下卖出页面。然后确认卖出，即可成功创建卖出订单。</p>
                        <img src="assets/img/help/3-04.png">

                        <p>5. 之后进入如下沟通页面，与买家联系，提供你的收款信息。</p>
                        <img src="assets/img/help/3-05.png">
                        
                        <p>6. 等买家付款后，登录银行账户查看钱款到账情况，确认无误后，点击“放行USDT”。</p>
                        <img src="assets/img/help/3-06.png">
                        <img src="assets/img/help/3-07.png">

                        <p>7. 放行后订单完成，恭喜你已经成功提现。</p>
                        <img src="assets/img/help/3-08.png">

                    </div>
                    <div class="col-sm-2 hidden-xs">
                        <!-- <?php include 'sub-nav.html' ?> -->
                        <ul class="topul">
                            <li>数字货币</li>
                            <ul class="subul">
                                <li><a href="/products/usdt.php">USDT</a></li>
                                <li><a href="/products/btc.php">比特币</a></li>
                                <li><a href="/products/eth.php">以太坊</a></li>
                                <li><a href="/products/ltc.php">莱特币</a></li>
                            </ul>
                            <!-- <li>法律中心</li>
                            <ul class="subul">
                                <li><a href="/open-account.php">用户协议</a></li>
                                <li><a href="/disclaimer.php">法律声明</a></li>
                                <li><a href="/policy.php">隐私条款</a></li>
                            </ul> -->
                            <li>联系我们</li>
                            <ul class="subul">
                                <li><a href="/contact.php">客户服务</a></li>
                                <li><a href="/consulting.php">合作咨询</a></li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script> -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script> -->

    <!-- <script src="assets/js/common.js"></script> -->
</body>
</html>