<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RHYBIT- 合作咨询</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">

    <style>
        .page {
            margin-top: 80px;
        }
        .page > nav {
            line-height: 80px;
            border-bottom: 1px solid #ccc;
        }
        .page > nav  ol.breadcrumb {
            margin-bottom: 0;
            padding: 0;
        }
        
        .sub-nav .topul {
            border-bottom: 3px solid #820f1a;
        }
        .sub-nav .topul > li{
            line-height: 60px;
            background: #cb1224;
            color: #fff;
            font-size: 14px;
            font-weight: bold;
            padding-left: 15px;
            /*border-radius: 3px;*/
        }
        .sub-nav .subul li a{
            display: block;
            line-height: 50px;
            color: #666;
            padding-left: 15px;
            border-top: 1px solid #fff;
            border-bottom: 1px solid #fff;
            text-decoration: none;
        }
        .sub-nav .subul li a:hover {
            background: #cb1224;
            color: #fff;
        }
        .main {
            padding: 50px 0;
        }
        .left h2 {
            margin-top: 50px;
            margin-bottom: 30px;
            font-size: 36px;
            letter-spacing: 3px;
            font-weight: bold;
        }
        .left form {
            margin-top: 30px;
        }
        .form-group label {
            position: relative;
            top: 5px;
        }

        @media (max-width: 768px) {
            .page {
                margin-top: 0;
            }

        }
    </style>

</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <nav>
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="/">首页</a></li>
                    <li><a href="/contact.php">客户需知</a></li>
                    <li class="active">合作咨询</li>
                </ol>
            </div>
        </nav>
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">

                        <div class="left">
                            <h2>合作咨询</h2>
                            <p>请留下您的联系方式，我们将在第一时间联系您。</p>
                            <form>
                                <fieldset>
                                    <div class="row form-group">
                                        <label for="inputName" class="col-sm-2 control-label">姓名</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputName" placeholder="请输入您的姓名">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="inputName" class="col-sm-2 control-label">性别</label>
                                        <div class="col-sm-8">
                                            <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> 男
                                            <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> 女
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="inputPhone" class="col-sm-2 control-label">联系方式</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputPhone" placeholder="请输入您的手机号码">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">邮箱</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail" placeholder="请输入您的邮箱">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="inputCity" class="col-sm-2 control-label">城市</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputCity" placeholder="请输入您的城市">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="inputInfo" class="col-sm-2 control-label">附加内容</label>
                                        <div class="col-sm-8">
                                          <textarea name="" class="form-control" id="inputInfo" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>


                                </fieldset>
                            </form>

                        </div>

                    </div>

                    <div class="col-sm-3 col-sm-offset-1 hidden-xs sub-nav">
                        <?php include 'sub-nav.html' ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script> -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script> -->

    <!-- <script src="assets/js/common.js"></script> -->
</body>
</html>