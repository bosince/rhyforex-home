<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RHYBIT- 法律声明</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="/assets/css/page-common.css">

   <!--  <style>
        .page {
            margin-top: 80px;
        }
        .page > nav {
            line-height: 80px;
            border-bottom: 1px solid #ccc;
        }
        .page > nav  ol.breadcrumb {
            margin-bottom: 0;
            padding: 0;
        }
        
        .sub-nav .topul {
            border-bottom: 3px solid #820f1a;
        }
        .sub-nav .topul > li{
            line-height: 60px;
            background: #cb1224;
            color: #fff;
            font-size: 14px;
            font-weight: bold;
            padding-left: 15px;
            /*border-radius: 3px;*/
        }
        .sub-nav .subul li a{
            display: block;
            line-height: 50px;
            color: #666;
            padding-left: 15px;
            border-top: 1px solid #fff;
            border-bottom: 1px solid #fff;
            text-decoration: none;
        }
        .sub-nav .subul li a:hover {
            background: #cb1224;
            color: #fff;
        }
        .main {
            padding: 50px 0;
        }
        .main h2 {
            margin-top: 0;
            margin-bottom: 30px;
            font-size: 36px;
            letter-spacing: 3px;
            font-weight: bold;
        }
        .main h4 {
            font-size: 16px;
            margin-top: 20px;
        }
        .main p {
            font-size: 14px;
            line-height: 28px;
        }

        @media (max-width: 768px) {
            .page {
                margin-top: 0;
            }

        }
    </style> -->

</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <nav>
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="/">首页</a></li>
                    <li><a href="/open-account.php">法律中心</a></li>
                    <li class="active">法律声明</li>
                </ol>
            </div>
        </nav>
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>法律声明</h2>
                        
                        <h4>第一条</h4>

                        <p>本网站的宗旨是在不违反塞舌尔共和国相关法律法规的前提下，尽可能地为全球广大数字资产爱好者及投资者提供专业的国际化水准的交易平台和金融产品。禁止使用本网站从事洗钱、走私、商业贿赂等一切非法交易活动，若发现此类事件，本站将冻结账户，立即报送有权机关。</p>

                        <h4>第二条</h4>

                        <p>当有权机关出示相应的调查文件要求本站配合对指定用户进行调查时， 或对用户账户采取查封、冻结或者划转等措施时，本站将按照有权机关的要求协助提供相应的用户数据，或进行相应的操作。 因此而造成的用户隐私泄露、账户不能操作及因此给所造成的损失等，本站不承担任何责任。</p>

                        <h4>第三条</h4>

                        <p>本网站使用者因为违反本声明的规定而触犯塞舌尔共和国相关法律的，本站作为服务的提供方，有义务对平台的规则及服务进行完善， 但本站并无触犯塞舌尔共和国相关法律的动机和事实，对使用者的行为不承担任何连带责任。</p>

                        <h4>第四条</h4>

                        <p>凡以任何方式登录本网站或直接、间接使用本网站服务者，视为自愿接受本网站声明的约束。</p>

                        <h4>第五条</h4>

                        <p>本声明未涉及的问题参见塞舌尔共和国有关法律法规，当本声明与塞舌尔共和国相关法律法规冲突时，以塞舌尔共和国相关法律法规为准。</p>


                    </div>

                    <div class="col-sm-3 col-sm-offset-1 hidden-xs sub-nav">
                        <?php include 'sub-nav.html' ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script> -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script> -->

    <!-- <script src="assets/js/common.js"></script> -->
</body>
</html>