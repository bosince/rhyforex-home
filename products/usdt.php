<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> USDT</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="/assets/css/common.css">
    <link rel="stylesheet" href="/assets/css/page-common.css">

</head>
<body>
    <?php include '../header.html' ?>

    <div class="page">
        <nav>
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="/">首页</a></li>
                    <li><a href="/products/usdt.php">数字货币</a></li>
                    <li class="active">USDT</li>
                </ol>
            </div>
        </nav>
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>USDT</h2>
                        <h4>1.什么是USDT</h4>
 
                        <p>USDT是Tether公司推出的基于稳定价值货币美元（USD）的代币Tether USD（下称USDT），1USDT=1美元，用户可以随时使用USDT与USD进行1:1兑换。Tether公司严格遵守1:1准备金保证，即每发行1个 USDT 代币，其银行账户都会有1美元的资金保障。用户可以在 Tether 平台进行资金查询，以保障透明度。用户可以通过SWIFT电汇美元至Tether公司提供的银行帐户，或通过交易所换取USDT。赎回美元时，反向操作即可。用户也可在交易所用比特币换取USDT。</p>

                        <p>USDT是在比特币区块链上发布的基于Omni Layer协议的数字资产。USDT最大的特点是，它与同数量的美元是等值的。USDT被设计为法币在数字网络上的复制品，使之成为波动剧烈的加密货币市场中良好的保值代币。</p>

                        <h4>2.特点</h4>

                        <p>直观： USDT与美元是等值的，1USDT=1美元。每个币种=多少USDT，也就相当于是它的单价是多少美元。</p>

                        <p>稳定：因为泰达币是由法币支撑的，用户可以在不受多数区块链资产价格波动影响的情况下仍然在区块链资产市场上进行交易。</p>

                        <p>透明： 泰达币的发行公司Tether 声称其法币储存账户有定期审计以确保市面流通的每一枚泰达币有对应的一美元为支撑。储存账户状态是公开的，可以随时查询到。此外，所有的泰达币交易记录都会公布在公链上。</p>

                        <p>小额交易费用： 在Tether账户间交易或者在储存有泰达币的钱包间交易都不收手续费。将泰达币转换为法定货币时需要收取交易服务费。</p>

                        <h4>3.详细参数</h4>
                        <img src="/assets/img/pro/usdt.png" alt="">

                        <h4>3.常用链接</h4>

                        <p>官方网站：https://tether.to/ </p>
                        <p>区块查询：https://blockchain.info/ </p>

                        

                    </div>

                    <div class="col-sm-3 col-sm-offset-1 hidden-xs sub-nav">
                        <?php include '../sub-nav.html' ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <?php include '../footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script> -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script> -->

    <!-- <script src="assets/js/common.js"></script> -->
</body>
</html>