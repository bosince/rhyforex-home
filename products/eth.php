<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> ETH(以太坊)</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="/assets/css/common.css">
    <link rel="stylesheet" href="/assets/css/page-common.css">

</head>
<body>
    <?php include '../header.html' ?>

    <div class="page">
        <nav>
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="/">首页</a></li>
                    <li><a href="/products/usdt.php">数字货币</a></li>
                    <li class="active">ETH(以太坊)</li>
                </ol>
            </div>
        </nav>
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>ETH(以太坊)</h2>
                        <h4>1.什么是以太坊？</h4>

                        <p>以太坊（Ethereum）是下一代密码学账本，可以支持众多的高级功能，包括用户发行货币，智能协议，去中心化的交易和设立去中心化自治组织(DAOs)或去中心化自治公司（DACs）。以太坊并不是把每一单个类型的功能作为特性来特别支持，相反，以太坊包括一个内置的图灵完备的脚本语言，允许通过被称为“合同”的机制来为自己想实现的特性写代码。一个合同就像一个自动的代理，每当接收到一笔交易，合同就会运行特定的一段代码，这段代码能修改合同内部的数据存储或者发送交易。高级的合同甚至能修改自身的代码。  </p> 
                         
                        <p>通俗一点说，以太坊是开源平台数字货币和区块链平台，它为开发者提供在区块链上搭建和发布应用的平台。以太坊可以用来编程、分散、担保和交易任何事物，投票、域名、金融交易所，众筹、公司管理、合同和大部分的协议、知识产权，还有得益于硬件集成的智能资产。</p>

                        <p>2017年2月28日，一批代表着石油、天然气行业，金融行业和软件开发公司的全球性企业正式推出企业以太坊联盟（EEA），致力于将以太坊开发成企业级区块链。这些企业包括英国石油巨头BP、摩根大通、软件开发商微软、埃森哲、桑坦德银行、BlockApps、BNY梅隆、芝商所、ConsenSys、英特尔微软和Nuco等。许多企业或组织开发基于以太坊区块链的项目，并发行代币。联合国世界粮食计划署成功使用以太坊区块链向10000难民提供救助。</p>

                        <h4>2.详细参数</h4>

                        <p>中文名：以太坊                     英文名：Ethereum        英文简称：ETH</p>
                        <p>研发者：Vitalik Buterin         核心算法：Ethash         发布日期：2015/3/20</p>
                        <p>区块时间：约15-17秒/块       区块奖励：5                  货币总量：7200万+1872万/年</p>
                        <p>主要特色：含数字货币和智能合约等特色功能</p>

                        <h4>3.常用链接</h4>

                        <p>官方网站：https://www.ethereum.org/</p>
                        <p>区块查询：https://etherscan.io/</p>

                        

                    </div>

                    <div class="col-sm-3 col-sm-offset-1 hidden-xs sub-nav">
                        <?php include '../sub-nav.html' ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <?php include '../footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script> -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script> -->

    <!-- <script src="assets/js/common.js"></script> -->
</body>
</html>