<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> LTC (莱特币)</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="/assets/css/common.css">
    <link rel="stylesheet" href="/assets/css/page-common.css">


</head>
<body>
    <?php include '../header.html' ?>

    <div class="page">
        <nav>
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="/">首页</a></li>
                    <li><a href="/products/usdt.php">数字货币</a></li>
                    <li class="active">LTC (莱特币)</li>
                </ol>
            </div>
        </nav>
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>LTC (莱特币)</h2>
                        <h4>1.什么是莱特币？</h4>

                        <p>莱特币诞生于2011年11月9日，被称为是“数字白银”。莱特币在技术上和比特币具有相同的实现原理。它是第一个基于Scrypt算法的网络数字货币，与比特币相比，莱特币拥有更快的交易确认时间，更高的网络交易容量和效率。莱特币现在拥有完整的产业链，充分的流动性，足以证明其是成熟、安全、稳定的商用金融系统。</p>

                        <h4>2.数据块链</h4>

                        <p>莱特币数据块链与其竞争者——比特币相比起来，能够处理更大的交易量。由于数据块的产出更加频繁，该网络可以支持更多的交易，并且在将来无需修改软件。 
                        因此，商家可以获得更快的交易确认，而且在销售大额商品时依然能够等待更多的交易确认。</p>

                        <h4>3.钱包加密</h4>

                        <p>钱包加密能够保证您钱包中私钥的安全，从而让您可以查看交易情况及账户余额，但是在您使用莱特币之前必须输入密码。该功能不仅能够防止病毒及木马的侵扰，同时还是支付之前，一项合法性的有效检查。</p>

                        <h4>4.挖矿奖励</h4>

                        <p>“矿工们”目前每数据块可以产出25个莱特币。每4年，生产的莱特币货币量将减少一半(每经过840000个数据块)。 </p>
                        <p>因此，莱特币网络生产的货币总量将是比特币的4倍，即8400万个莱特币。</p>

                        <p>莱特币的创造和转让基于一种开源的加密协议，不受任何中央机构的管理。莱特币旨在改比特币，与其相比，莱特币具有三种显著差异：</p>

                        <p>（1）莱特币网络每2.5分钟（而不是10分钟）就可以处理一个块，因此可以提供更快的交易确认。</p>

                        <p>（2）莱特币网络预期产出8400万个，是比特币网络发行货币量的四倍之多。</p>

                        <p>（3）莱特币在其工作量证明算法中使用了由Colin Percival首次提出的scrypt加密算法。</p>

                        <p>2017年4 月，莱特币社区经过投票达成协议，决定通过隔离验证软分叉对其区块链进行升级。同年6月，莱特币闪电网络正式上线。</p>

                        <h4>5.详细参数</h4>

                        <p>中文名：莱特币             英文名：Litecoin            英文简称：LTC</p>

                        <p>研发者：Charlie Lee     核心算法：Scrypt            发布日期：2011/10/7</p>

                        <p>区块时间：150秒/块     发行总量：8400万           减半时间：4年</p>

                        <p>共识证明：POW           难度调整：2016个区块     区块奖励：最初50LTC，当前25LTC</p>

                        <p>主要特色：发行量大：发行量是比特币的4倍；交易快速：确认时间仅2.5分钟；隔离验证激活</p>

                        <p>不足之处：莱特币相关应用还较少</p>

                        <p>风险：隔离验证激活后，网络漏洞的风险</p>

                        <h4>6.常用链接</h4>

                        <p>官方网站：https://litecoin.org/</p>

                        <p>区块查询：http://explorer.litecoin.net/</p>

                        

                    </div>

                    <div class="col-sm-3 col-sm-offset-1 hidden-xs sub-nav">
                        <?php include '../sub-nav.html' ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <?php include '../footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script> -->
    <!-- <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script> -->

    <!-- <script src="assets/js/common.js"></script> -->
</body>
</html>