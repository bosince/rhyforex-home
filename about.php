<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RHYBIT - 关于我们</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/about.css">

</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner">
        <div class="container">
            <div class="banner-info visible-lg">
                <h1>RHYBIT</h1>
                <p>RHYBIT（Rhine  Group Limited）总部位于塞舌尔，RHYBIT是一家面向全球发展的国际顶尖数字资产服务平台，长期以来集中所有资源，全力投身于数字资产行业中。RHYBIT旨在为全球客户提供多层次、多元化、全方位的数字资产服务，打造世界级的一流交易平台。</p>

                <p>RHYBIT高起点、注重口碑信誉，在英国、澳大利亚、美国等多地设立办事处。我们提供24小时比特币、以太坊、莱特币等多元化数字资产在线交易，为客户提供方便快捷的数字资产服务。最直观的表现方式为执行最优的交易环境、享受超低交易成本。</p>
            </div>
            <div class="banner-list-wrap visible-lg">
                <div class="banner-list-item banner-list-item1">
                    <h4>愿景</h4>
                    <p>以科技创造价值，做全球醒来的专业化数字资产交易平台</p>
                </div>
                <div class="banner-list-item banner-list-item2">
                    <h4>目标</h4>
                    <p>打造安全诚信的全球交易平台，为客户提供稳定的交易环境和更好的服务</p>
                </div>
                <div class="banner-list-item banner-list-item3">
                    <h4>价值观</h4>
                    <p>双赢，构建信誉升级</p>
                </div>
                <div class="banner-list-item banner-list-item4">
                    <h4>使命</h4>
                    <p>建立客户对数字货币的信任和信息，为客户创造价值</p>
                </div>
            </div>
        </div>
    </section>
    <section class="m-banner-info hidden-lg">
        <div class="m-banner-info1">
            <h1>RHYBIT</h1>
            <p>RHYBIT（Rhine  Group Limited）总部位于塞舌尔，RHYBIT是一家面向全球发展的国际顶尖数字资产服务平台，长期以来集中所有资源，全力投身于数字资产行业中。RHYBIT旨在为全球客户提供多层次、多元化、全方位的数字资产服务，打造世界级的一流交易平台。</p>

            <p>RHYBIT高起点、注重口碑信誉，在英国、澳大利亚、美国等多地设立办事处。我们提供24小时比特币、以太坊、莱特币等多元化数字资产在线交易，为客户提供方便快捷的数字资产服务。最直观的表现方式为执行最优的交易环境、享受超低交易成本。</p>
        </div>
        <ul class="m-banner-info2">
            <li>
                <h4>愿景</h4>
                <p>以科技创造价值，做全球醒来的专业化数字资产交易平台</p>
            </li>
            <li>
                <h4>目标</h4>
                <p>打造安全诚信的全球交易平台，为客户提供稳定的交易环境和更好的服务</p>
            </li>
            <li>
                <h4>价值观</h4>
                <p>双赢，构建信誉升级</p>
            </li>
            <li>
                <h4>使命</h4>
                <p>建立客户对数字货币的信任和信息，为客户创造价值</p>
            </li>
        </ul>
    </section>

    <section class="supervise">
        <div class="container">
            <!-- <div class="supervise-info">
                <h1>NFA 监管</h1>
                <p>RHYBIT受美国全国期货协会(National Futures Association—NFA)授权并监管，监管牌照号：0509259。我们严格遵守NFA的资金规定，公司与客户资金严格分离。</p>
            </div>
            <div class="supervise-img hidden-xs">
                <img src="assets/img/about/nfa.png" alt="">
            </div> -->
        </div>
    </section>

    <div class="safe">
        <div class="safe-img hidden-xs">
        </div>
        <div class="safe-info">
            <h1>资金安全</h1>
            <p>RHYBIT采用国际银行级用户数据加密，多重签名冷钱包风险多级控制，保障用户资产安全，并且，安全稳定运营数字资产交易所数年，管理资产规模超过10亿美金，服务数百万用户，专业分布式架构和防DDOS攻击系统，98% 数字资产存储多重签名冷钱包；</p>
        </div>
    </div>

    <div class="plan">
        <div class="plan-info">
            <h1>SMART-Chain资产评估模型</h1>
            <p>独立专业的区块链资产研究评估体系</p>
<p>长期跟踪产业链并提供最权威中立的资产分析</p>
<p>一站式的项目进度跟踪及信息披露系统</p>
        </div>
        <div class="plan-space hidden-xs hidden-sm"></div>
    </div>
    <div class="partner">
        <ul>
            <li><img src="assets/img/about/partner01.jpg" alt=""></li>
            <li><img src="assets/img/about/partner02.jpg" alt=""></li>
            <li><img src="assets/img/about/partner03.jpg" alt=""></li>
            <li><img src="assets/img/about/partner04.jpg" alt=""></li>
            <li><img src="assets/img/about/partner05.jpg" alt=""></li>
        </ul>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>