<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RHYBIT</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="//cdn.bootcss.com/animate.css/3.5.2/animate.min.css" >
    <link rel="stylesheet" href="https://cdn.bootcss.com/hover.css/2.1.1/css/hover-min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/index.css">
    
</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner">
        <div class="banner-info">
            <!-- <h1>一个账户 投资全球</h1> -->
            <h2>依托RHYBIT数年的数字资产安全风控经验</h2>
            <p>安全稳定运营数字资产交易所超过四年; </p>
            <p>管理资产规模超过10亿美金，服务数百万用户;</p>
            <p>专业分布式架构和防DDOS攻击系统;</p>
            <p>98% 数字资产存储多重签名冷钱包</p>
        </div>

        <div class="circle hidden-xs hidden-sm">
            <figure>
                <img src="assets/img/stock_exchange2.png" alt="">
            </figure>
        </div>
    </section>

    <!-- <section class="product">
        <h1 class="module">RHYBIT产品</h1>
        <p class="module">更多的交易机会，更好的投资方案</p>
        <div id="featured-area" class="visible-lg"> 
            <ul> 
                <li>
                    <img src="assets/img/pro1.jpg" alt="" />
                    <div class="info">
                        外汇
                    </div>
                    <div class="active-info magictime swap">
                        <img src="assets/img/pro1_icon.png" alt="">
                        <h3>外汇</h3>
                        <p>外汇交易市场是全球最大的金融产品市场，也是世界上最活跃与最具流动性的金融产品，每日有超过5万亿美金的资本流动。</p>
                        <a href="#">创建账户</a>
                    </div>

                </li>
                <li>
                    <img src="assets/img/pro2.jpg" alt="" />
                    <div class="info">
                        指数
                    </div>
                    <div class="active-info magictime swap ">
                        <img src="assets/img/pro2_icon.png" alt="">
                        <h3>指数</h3>
                        <p>股指CFD是以股票指数为标的物，对股票投资者来说是非常有效的套期保值避险工具。</p>
                        <a href="#">创建账户</a>
                    </div>
                </li>  
                <li>
                    <img src="assets/img/pro3.jpg" alt="" />
                    <div class="info">
                        数字<br>货币
                    </div>
                    <div class="active-info magictime swap">
                        <img src="assets/img/pro3_icon.png" alt="">
                        <h3>数字货币</h3>
                        <p>数字货币市场实行的是无涨跌幅限制、不限日内交易次数，可做多做空，可选择的优质标的较多。</p>
                        <a href="#">创建账户</a>
                    </div>
                </li>  
            </ul> 
        </div>
        <ul class="m-product-wrap hidden-lg"> 
            <li>
                <div>
                    <img src="assets/img/pro1.jpg" alt="" />
                    <div class="m-product-info magictime swap">
                        <img src="assets/img/pro1_icon.png" alt="">
                        <h3>外汇</h3>
                        <p>外汇交易市场是全球最大的金融产品市场，也是世界上最活跃与最具流动性的金融产品，每日有超过5万亿美金的资本流动。</p>
                        <a href="#">创建账户</a>
                    </div>
                </div>
                
            </li>
            <li>
                <div>
                    <img src="assets/img/pro2.jpg" alt="" />

                    <div class="m-product-info magictime swap ">
                        <img src="assets/img/pro2_icon.png" alt="">
                        <h3>指数</h3>
                        <p>股指CFD是以股票指数为标的物，对股票投资者来说是非常有效的套期保值避险工具。</p>
                        <a href="#">创建账户</a>
                    </div>
                </div>
            </li>  
            <li>
                <div>
                    <img src="assets/img/pro3.jpg" alt="" />

                    <div class="m-product-info magictime swap">
                        <img src="assets/img/pro3_icon.png" alt="">
                        <h3>数字货币</h3>
                        <p>交易订单在投资者之间撮合成交，交易所不参与交易环节，而产品报价由国际市场行情报价决定。</p>
                        <a href="#">创建账户</a>
                    </div>
                </div>
            </li>  
        </ul> 
    </section> -->


    <section class="adv0 hidden-sm hidden-xs">
        <div class="adv0-lists">
            <div class="adv0-item adv0-item1">
                <img src="assets/img/adv0/adv0-1.png" alt="">
                <h4>极致的用户体验</h4>
                <p>用心雕琢用户的使用习惯，多终端掌握交易行情，美妙十万单高性能交易引擎，确保交易过程快捷</p>
            </div>
            <div class="adv0-item adv0-item2">
                <img src="assets/img/adv0/adv0-2.png" alt="">
                <h4>银行级安全防护</h4>
                <p>采用银行级用户数据加密，多重签名冷钱包风险多级控制，保障用户资产安全</p>
            </div>
            <div class="adv0-item adv0-item3">
                <img src="assets/img/adv0/adv0-3.png" alt="">
                <h4>顶级技术团队打造</h4>
                <p>聚集众多行业精英，一流技术团队专业打造十年金融安全经验团队，全称为您保驾护航</p>
            </div>
            <div class="adv0-item adv0-item4">
                <img src="assets/img/adv0/adv0-4.png" alt="">
                <h4>全天候在线服务</h4>
                <p>全年7*24小时客户服务，5分钟内响应工单请求，1小时内响应邮件服务，秉承客户至上</p>
            </div>
            <h3>全球领先的数字资产交易平台</h3>
        </div>
    </section>
    <section class="adv0-m visible-xs visible-sm">
        <h3>全球领先的数字资产交易平台</h3>
        <div class="container">
            <div class="col-xs-6">  
                <div class="adv0-m-item adv0-m-item1">
                    <img src="assets/img/adv0/adv0-1.png" alt="">
                    <h4>极致的用户体验</h4>
                    <p>用心雕琢用户的使用习惯，多终端掌握交易行情，美妙十万单高性能交易引擎，确保交易过程快捷</p>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="adv0-m-item adv0-m-item2">
                    <img src="assets/img/adv0/adv0-2.png" alt="">
                    <h4>银行级安全防护</h4>
                    <p>采用银行级用户数据加密，多重签名冷钱包风险多级控制，保障用户资产安全</p>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="adv0-m-item adv0-m-item3">
                    <img src="assets/img/adv0/adv0-3.png" alt="">
                    <h4>顶级技术团队打造</h4>
                    <p>聚集众多行业精英，一流技术团队专业打造十年金融安全经验团队，全称为您保驾护航</p>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="adv0-m-item adv0-m-item4">
                    <img src="assets/img/adv0/adv0-4.png" alt="">
                    <h4>全天候在线服务</h4>
                    <p>全年7*24小时客户服务，5分钟内响应工单请求，1小时内响应邮件服务，秉承客户至上</p>
                </div>
            </div>
        </div>
    </section>


    <section class="adv">
        <h1 class="module">八大优势，引领交易新方向</h1>
        <p class="module">多个领先于行业的技术优势助力顺畅交易</p>
        <div class="container advs-wrap">
            <div class="row ">
                <!-- <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv01.png" alt="">
                        <h4>RHY Trader</h4>
                        <p>独立研发、可交易港股和美股、操作简单</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv02.png" alt="">
                        <h4>720°支持</h4>
                        <p>可复制模式、顶尖团队、成功案例、一对一服务</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv03.png" alt="">
                        <h4>流动性提供商LP</h4>
                        <p>直接接入顶级LP、 更优报价、及时成交</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv04.png" alt="">
                        <h4>产品多样</h4>
                        <p>证券、外汇、贵金属、指数</p>
                    </div>
                </div> -->

                <!-- line 2 -->
                <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv05.png" alt="">
                        <h4>快捷方便</h4>
                        <p>充币即时，提现迅速，每秒万单的高性能交易引擎</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv06.png" alt="">
                        <h4>杠杆灵活</h4>
                        <p>高达1:10</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv07.png" alt="">
                        <h4>资金安全</h4>
                        <p>采用银行级用户数据加密，风险多级控制，保障用户资产安全</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv08.png" alt="">
                        <h4>贴心</h4>
                        <p>精选优质币种，细节优化处理操作体验更佳</p>
                    </div>
                </div>

                <!-- line 3 -->
                <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv09.png" alt="">
                        <h4>稳健交易环境</h4>
                        <p>投入、庞大的服务器、 专业的技术团队 、独立研发技术</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv10.png" alt="">
                        <h4>CRM系统</h4>
                        <p>精准数据 、操作简单 、功能完善</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv11.png" alt="">
                        <h4>公开透明</h4>
                        <p>安全可靠、规则透明，采用开源项目</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv/adv12.png" alt="">
                        <h4>高流动性</h4>
                        <p>我们的用户来自于世界100多个国家，交易量十分活跃</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="banner2">
        <div class="container">
            <div class="banner2-top">
                <h1 class="module">全球化的数字资产配置及交易服务</h1>
                <!-- <p class="module">更优报价，急速成交，让你的交易更高效</p> -->
            </div>
            <div class="banner2-bottom">
                <p>遍布全球的项目拓展及运营管理体系; 在多个国家和地区设有本地交易服务中心; 服务超过130个国家的数百万用户</p>
            </div>
        </div>
    </section>

    <section class="highly-active">
        <div class="container">
            <h1 class="module">多平台终端接入</h1>
            <p>覆盖iOS、Android、Windows、Mac多个平台，支持全业务功能</p>
            <!-- <p>MetaTrader4 是用于外汇，金属和期货市场交易的先进交易平台。</p>
            <p>MetaTrader4 是世界上最受欢迎的交易客户终端，它提供了交易外汇，金属，指数和商品的必要功能。</p> -->
            <img src="assets/img/downApp.png" alt="">
            
            <div class="highly-info">
                <div class="infoL">
                    <h4>RHYBIT Trader</h4>
                    <p>一个平台，投资全球；独立研发，交易保障；更优报价，促进成交；一站式24小时服务；加密保护，稳健交易</p>
                    <a href="mt4.php">了解更多</a>
                </div>
                <div class="infoR">
                    <h4>MT4</h4>
                    <p>多语言支持；即时报价，及时买卖；支持多种订单管理；高效率、高性能、高可靠性和安全性</p>
                    <a href="mt4.php">了解更多</a>
                </div>
            </div>

            <!-- <div class="highly-active-btns">
                <a href="https://download.mql5.com/cdn/web/10737/mt4/RHYforex4setup.exe">下载MT4</a>
                <a href="../static/RHY_bin_setup.exe">下载期权插件</a>
            </div> -->
        </div>
    </section>


    <section class="contact">
        <div class="container">
            <h1>有任何问题？即刻联系我们！</h1>
            <p>我们有经验丰富的客户经理和技术支持人员，随时协助您解决任何问题。</p>
            <a href="contact.php">联系我们</a>
        </div>
    </section>


    <?php include 'footer.html' ?>
    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/jquery.roundabout.js"></script>
    <script src="assets/js/jquery.easing.1.3.js"></script>
    <script src="assets/js/common.js"></script>


    <script>
        $(document).ready(function(){ 
            $('#featured-area ul').roundabout({
                duration: 800,
                easing: 'swing',
                autoplay: true,
                minOpacity: 1,
                minScale: 0.6
            },function(){
                $(".roundabout-moveable-item:last-child").find(".info").css({
                    left: '0',
                    right: 'auto'
                });
            });

            
            $("#featured-area").on('animationStart', function(event) {
                $(".roundabout-in-focus").find(".info").css({
                    left: '0',
                    right: 'auto'
                }).end().siblings('.roundabout-moveable-item').find(".info").css({
                    right: '0',
                    left: 'auto'
                });
            });
            
            
            // $(".adv-item").hover(function() {
            //     $(this).find('.hover-hidden').slideUp(400).siblings('.hover-show').slideDown(400);
            // }, function() {
            //     $(this).find('.hover-hidden').slideDown(400).siblings('.hover-show').slideUp(400);
            // });

        });
    </script>
    
</body>
</html>